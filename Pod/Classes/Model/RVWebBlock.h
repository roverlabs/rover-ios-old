//
//  RVWebBlock.h
//  Pods
//
//  Created by Ata Namvari on 2016-03-01.
//
//

#import "RVBlock.h"

@interface RVWebBlock : RVBlock

@property (nonatomic, strong) NSURL *webURL;
@property (nonatomic, assign) CGFloat height;

@end
