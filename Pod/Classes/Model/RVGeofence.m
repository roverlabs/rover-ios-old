//
//  RVGeofence.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-21.
//
//

#import "RVGeofence.h"
@import CoreLocation;

@implementation RVGeofence

- (CLCircularRegion *)CLCircularRegion {
    return [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(self.latitude.doubleValue, self.longitude.doubleValue) radius:self.radius.doubleValue identifier:self.ID];
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    
    [encoder encodeObject:self.latitude forKey:@"latitude"];
    [encoder encodeObject:self.longitude forKey:@"longitude"];
    [encoder encodeObject:self.radius forKey:@"radius"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super initWithCoder:decoder])) {
        self.latitude = [decoder decodeObjectForKey:@"latitude"];
        self.longitude = [decoder decodeObjectForKey:@"longitude"];
        self.radius = [decoder decodeObjectForKey:@"radius"];
    }
    return self;
}

@end
