//
//  RVWebBlock.m
//  Pods
//
//  Created by Ata Namvari on 2016-03-01.
//
//

#import "RVWebBlock.h"

@implementation RVWebBlock

- (CGFloat)heightForWidth:(CGFloat)width {
    return [super heightForWidth:width] + self.height;
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)encoder {
    [super encodeWithCoder:encoder];
    
    [encoder encodeObject:self.webURL forKey:@"webURL"];
    [encoder encodeObject:[NSNumber numberWithFloat:self.height] forKey:@"height"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super initWithCoder:decoder])) {
        self.webURL = [decoder decodeObjectForKey:@"webURL"];
        self.height = [[decoder decodeObjectForKey:@"height"] floatValue];
    }
    return self;
}

@end
