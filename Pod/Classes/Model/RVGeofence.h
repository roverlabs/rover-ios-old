//
//  RVGeofence.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-21.
//
//

#import "RVModel.h"

@class CLCircularRegion;

@interface RVGeofence : RVModel

@property (nonatomic, strong) NSNumber *latitude;
@property (nonatomic, strong) NSNumber *longitude;
@property (nonatomic, strong) NSNumber *radius;

- (CLCircularRegion *)CLCircularRegion;

@end
