//
//  RSCode128Generator.h
//  RSBarcodes
//
//  Created by R0CKSTAR on 12/30/13.
//  Copyright (c) 2013 P.D.Q. All rights reserved.
//

#import "RXCodeGenerator.h"

typedef NS_ENUM(NSUInteger, RXCode128GeneratorCodeTable) {
    RXCode128GeneratorCodeTableAuto = 0,
    RXCode128GeneratorCodeTableA = 1,
    RXCode128GeneratorCodeTableB = 2,
    RXCode128GeneratorCodeTableC = 3
};

/**
 *  http://www.barcodeisland.com/code128.phtml
 *  http://courses.cs.washington.edu/courses/cse370/01au/minirproject/BarcodeBattlers/barcodes.html
 */
@interface RXCode128Generator : RXAbstractCodeGenerator <RXCheckDigitGenerator>

- (id)initWithContents:(NSString *)contents;

@property(nonatomic) RXCode128GeneratorCodeTable codeTable;

@end
