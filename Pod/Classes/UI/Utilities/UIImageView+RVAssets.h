//
//  UIImageView+RVAssets.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import <UIKit/UIKit.h>

@interface UIImageView (RVAssets)

- (void)rv_setImageWithURL:(NSURL *)url;
- (void)rv_setImageWithURL:(NSURL *)url usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;

@end
