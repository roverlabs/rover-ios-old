//
//  RVAssetCache.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import "RVAssetCache.h"
@import UIKit;

@interface RVAssetCache ()

@property (nonatomic, strong) NSCache *memCache;
@property (nonatomic, strong) dispatch_queue_t ioQueue;
@property (nonatomic, strong) NSString *diskCachePath;

@end

@implementation RVAssetCache {
    NSFileManager *_fileManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _memCache = [[NSCache alloc] init];
        _memCache.name = @"co.roverlabs.RVAssetCache";
        
        _ioQueue = dispatch_queue_create("co.roverlabs.RVAssetCache", DISPATCH_QUEUE_SERIAL);
        
        dispatch_sync(_ioQueue, ^{
            _fileManager = [NSFileManager new];
        });
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        _diskCachePath = [paths[0] stringByAppendingPathComponent:@"RVAssetCache"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearInMemoryCache) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)hasInMemoryCacheForKey:(NSString *)key {
    NSData *data = [_memCache objectForKey:key];
    if (data) {
        return YES;
    }
    return NO;
}

- (void)setAssetWithData:(NSData *)data forKey:(NSString *)key {
    if (!data || !key) {
        return;
    }
    
    // Store in memory
    [_memCache setObject:data forKey:key cost:data.length];

    // Store on disk
    dispatch_async(_ioQueue, ^{
        if (![_fileManager fileExistsAtPath:_diskCachePath]) {
            [_fileManager createDirectoryAtPath:_diskCachePath withIntermediateDirectories:YES attributes:nil error:NULL];
        }
        
        NSString *filePath = [self filePathForAssetWithKey:key];
        [_fileManager createFileAtPath:filePath contents:data attributes:nil];
        
        
        // Disable iCloud backup
        NSURL *fileURL = [NSURL fileURLWithPath:filePath];
        [fileURL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:nil];
    });
}

- (NSString *)filePathForAssetWithKey:(NSString *)key {
    return [_diskCachePath stringByAppendingPathComponent:key];
}

- (NSData *)inMemoryCachedDataForKey:(NSString *)key {
    
    // Check in memory cache first
    NSData *data = [_memCache objectForKey:key];
    if (data) {
        return data;
    }
    
    return nil;
}

- (void)clearInMemoryCache {
    [_memCache removeAllObjects];
}

- (void)clearDiskCache {
    dispatch_async(_ioQueue, ^{
        [_fileManager removeItemAtPath:_diskCachePath error:nil];
        [_fileManager createDirectoryAtPath:_diskCachePath withIntermediateDirectories:YES attributes:nil error:NULL];
    });
}

- (void)clearCache {
    [self clearInMemoryCache];
    [self clearDiskCache];
}

- (void)queryDiskCacheForKey:(NSString *)key completion:(void (^)(NSData *, NSError *))completion {
    dispatch_async(_ioQueue, ^{
        
        NSString *filePath = [self filePathForAssetWithKey:key];
        
        if (![_fileManager fileExistsAtPath:filePath]) {
            if (completion) {
                NSError *error = [NSError errorWithDomain:@"co.roverlabs.RVAssetCache" code:ENOENT userInfo:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, error);
                });
            }
            return;
        }
        
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        if (data && completion) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(data, nil);
            });
        }
        
    });
}

@end
