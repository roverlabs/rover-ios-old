//
//  RVAssetManager.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import <Foundation/Foundation.h>

@interface RVAssetManager : NSObject

+ (instancetype)sharedManager;

- (void)fetchAssetAtURL:(NSURL *)url completion:(void (^)(NSData *data))completion;

- (BOOL)cachedAssetExistsForURL:(NSURL *)url;
- (void)saveAssetToCache:(NSData *)data forURL:(NSURL *)url;
- (void)clearCache;

@end
