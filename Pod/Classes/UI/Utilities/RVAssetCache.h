//
//  RVAssetCache.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import <Foundation/Foundation.h>

@interface RVAssetCache : NSObject


- (void)setAssetWithData:(NSData *)data forKey:(NSString *)key;
- (NSData *)inMemoryCachedDataForKey:(NSString *)key;
- (void)clearCache;

- (BOOL)hasInMemoryCacheForKey:(NSString *)key;
- (void)queryDiskCacheForKey:(NSString *)key completion:(void(^)(NSData *data, NSError *error))completion;

@end
