//
//  UIImageView+RVAssets.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import "UIImageView+RVAssets.h"
#import "RVAssetManager.h"

@implementation UIImageView (RVAssets)

- (void)rv_setImageWithURL:(NSURL *)url {
    RVAssetManager *assetManager = [RVAssetManager sharedManager];
    [assetManager fetchAssetAtURL:url completion:^(NSData *data) {
        UIImage *image = [UIImage imageWithData:data];
        [self setImage:image];
    }];
}

- (void)rv_setImageWithURL:(NSURL *)url usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle {
    RVAssetManager *assetManager = [RVAssetManager sharedManager];
    UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityStyle];
    activityIndicatorView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self addSubview:activityIndicatorView];
    
    [self addConstraints:@[[NSLayoutConstraint constraintWithItem:activityIndicatorView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0],
                           [NSLayoutConstraint constraintWithItem:activityIndicatorView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]]];
    
    [activityIndicatorView startAnimating];
    
    [assetManager fetchAssetAtURL:url completion:^(NSData *data) {
            
            UIImage *image = [UIImage imageWithData:data];
            [self setImage:image];
            
            [activityIndicatorView removeFromSuperview];
        
    }];
}

@end
