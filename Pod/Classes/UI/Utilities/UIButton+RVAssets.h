//
//  UIButton+RVAssets.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import <UIKit/UIKit.h>

@interface UIButton (RVAssets)

- (void)rv_setBackgroundImageWithURL:(NSURL *)url forState:(UIControlState)state;

@end
