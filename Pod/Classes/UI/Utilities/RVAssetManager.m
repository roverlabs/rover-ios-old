//
//  RVAssetManager.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import "RVAssetManager.h"
#import "RVAssetCache.h"
#import "RVGlobal.h"

@interface RVAssetManager () <NSURLSessionDataDelegate>

@property (nonatomic, strong) RVAssetCache *cache;
@property (nonatomic, strong) NSURLSession *urlSession;

@end

@implementation RVAssetManager

static RVAssetManager *sharedInstance = nil;
+ (instancetype)sharedManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _cache = [[RVAssetCache alloc] init];
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        _urlSession = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
        
    }
    return self;
}

- (void)fetchAssetAtURL:(NSURL *)url completion:(void (^)(NSData *))completion {
    NSString *key = [self cacheKeyForURL:url];
    
    NSData *data = [_cache inMemoryCachedDataForKey:key];
    if (data) {
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                RV_LTRACE(@"Asset found on memory cache: %@", url.path);
                completion(data);
            });
        }
        return;
    }
    
    NSURLSessionDataTask *dataTask = [_urlSession dataTaskWithURL:url
                                                completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                    if (error) {
                                                        RV_LERR(@"Could not download asset at URL: %@", url);
                                                    } else {
                                                        [_cache setAssetWithData:data forKey:[self cacheKeyForURL:url]];
                                                    }
                                                    
                                                    if (completion) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            RV_LTRACE(@"Downloaded asset: %@", url.path);
                                                            completion(data);
                                                        });
                                                    }
                                                }];
    
    [_cache queryDiskCacheForKey:key completion:^(NSData *data, NSError *error) {
        if (data) {
            if (completion) {
                RV_LTRACE(@"Asset found on disk cache: %@", url.path);
                completion(data);
            }
            return;
        }
    
        [dataTask resume];
    }];
    

}

- (BOOL)cachedAssetExistsForURL:(NSURL *)url {
    return [_cache hasInMemoryCacheForKey:[self cacheKeyForURL:url]];
}

- (void)saveAssetToCache:(NSData *)data forURL:(NSURL *)url {
    [_cache setAssetWithData:data forKey:[self cacheKeyForURL:url]];
}

- (void)clearCache {
    [_cache clearCache];
}

#pragma mark - Helpers

- (NSString *)cacheKeyForURL:(NSURL *)url {
    return url.lastPathComponent;
}

@end
