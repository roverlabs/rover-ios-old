//
//  RVAssetPrefetcher.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import "RVAssetPrefetcher.h"
#import "RVAssetManager.h"
#import "RVGlobal.h"

@interface RVAssetPrefetcher () <NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSURLSession *urlSession;

@end

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

@implementation RVAssetPrefetcher

+ (instancetype)sharedAssetPrefetcher {
    static id sharedPrefetcher = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPrefetcher = [[self alloc] init];
    });
    return sharedPrefetcher;
}



- (instancetype)init {
    if (self = [super init]) {
        NSURLSessionConfiguration *configuration;
        
        // if iOS 7
        // configuration  = blah balh balh
        if ([NSURLSessionConfiguration respondsToSelector:@selector(backgroundSessionConfigurationWithIdentifier:)]) {
            configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"roverBacgroundAssetPrefetchSession"];
        } else {
            configuration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"roverBacgroundAssetPrefetchSession"];
        }
        
        self.urlSession = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    }
    return self;
}

- (void)prefetchURLs:(NSArray *)urls {
    RV_LTRACE(@"Prefetching assets at urls: %@", urls);
    
    RVAssetManager *assetManager = [RVAssetManager sharedManager];
    [urls enumerateObjectsUsingBlock:^(NSURL *url, NSUInteger idx, BOOL *stop) {
        BOOL cacheExists = [assetManager cachedAssetExistsForURL:url];
        if (!cacheExists) {
            
            NSURLSessionDownloadTask *downloadTask = [self.urlSession downloadTaskWithURL:url];
            
            //[self.downloadTasks setObject:@(downloadTask.taskIdentifier) forKey:url];
            
            [downloadTask resume];
        }
    }];
}

#pragma mark - NSURLSessionDownloadTaskDelegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSData *imageData = [NSData dataWithContentsOfURL:location];
    [[RVAssetManager sharedManager] saveAssetToCache:imageData forURL:downloadTask.originalRequest.URL];
    
    RV_LTRACE(@"Asset downloaded - %lu BYTES - : %@", (unsigned long)imageData.length, downloadTask.originalRequest.URL.path);
}

@end