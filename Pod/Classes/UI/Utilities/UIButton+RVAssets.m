//
//  UIButton+RVAssets.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-27.
//
//

#import "UIButton+RVAssets.h"
#import "RVAssetManager.h"

@implementation UIButton (RVAssets)

- (void)rv_setBackgroundImageWithURL:(NSURL *)url forState:(UIControlState)state {
    RVAssetManager *assetManager = [RVAssetManager sharedManager];
    [assetManager fetchAssetAtURL:url completion:^(NSData *data) {
        UIImage *image = [UIImage imageWithData:data];
        [self setBackgroundImage:image forState:state];
    }];
}

@end
