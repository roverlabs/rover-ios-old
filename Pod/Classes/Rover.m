//
//  Rover.m
//  Rover
//
//  Created by Sean Rucker on 2014-06-23.
//  Copyright (c) 2014 Rover Labs Inc. All rights reserved.
//

#import "Rover.h"

#import "RVGlobal.h"

#import "RXFixedViewController.h"
#import "RVAssetManager.h"


@interface Rover () <RVVisitManagerDelegate, RXVisitViewControllerDelegate, RVLocationManagerDelegate>

@property (readonly, strong, nonatomic) RVConfig *config;
@property (nonatomic, strong) RVVisitManager *visitManager;
@property (nonatomic, strong) RVLocationManager *locationManager;
@property (nonatomic, strong) RVVisit *currentVisit;
@property (nonatomic, strong) id<RoverDelegate> defaultDelegate;
@property (nonatomic, strong) UIViewController *modalViewController;
@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, strong) UIWindow *applicationKeyWindow;

@end

@implementation Rover {
    RVCustomer *_customer;
    id<RoverDelegate> _delegate;
}

#pragma mark - Class methods

static Rover *sharedInstance = nil;

+ (Rover *)setup:(RVConfig *)config {
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        Class userSettingClass = NSClassFromString(@"UIUserNotificationSettings");
        if (userSettingClass) {
            [application registerUserNotificationSettings:[userSettingClass settingsForTypes:config.allowedUserNotificationTypes categories:nil]];
        }
    }
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithConfig:config];
    });
    
    return sharedInstance;
}

+ (Rover *)shared {
    if (sharedInstance == nil) {
        RV_LWARN(@"`shared` called before `setup:`");
    }
    return sharedInstance;
}

#pragma mark - Properties

- (RVCustomer *)customer {
    if (_customer) {
        return _customer;
    }
    
    _customer = [RVCustomer cachedCustomer];
    return _customer;
}

- (RVVisit *)currentVisit {
    if (_visitManager.latestVisit.isAlive) {
        return _visitManager.latestVisit;
    }
    
    return nil;
}

- (id<RoverDelegate>)delegate {
    if (_delegate) {
        return _delegate;
    }
    
    if (self.config.experience == RVExperienceNearby) {
        _defaultDelegate = [RVNearbyExperience new];
    } else if (self.config.experience == RVExperienceMessageFeed) {
        _defaultDelegate = [RVMessageFeedExperience new];
    }
    
    _delegate = _defaultDelegate;
    
    RV_LTRACE(@"Set roverDelegate: %@", _delegate);
    
    return _delegate;
}

- (void)setDelegate:(id<RoverDelegate>)delegate {
    _defaultDelegate = nil;
    _delegate = delegate;
}

#pragma mark - Initialization

- (instancetype)initWithConfig:(RVConfig *)config {
    self = [super init];
    if (self) {
        rvLogLevel = config.loggingLevel;
        
        _config = config;
        
        if (config.gimbalMode) {
            RV_LTRACE(@"Setting up in Gimbal mode");
            // Gimbal
            _visitManager = (RVVisitManager *)[[RVGimbalVisitManager alloc] init];
        } else {
            RV_LTRACE(@"Setting up in iBeacon mode");
            // Rover
            _visitManager = [[RVVisitManager alloc] init];
            
            _locationManager = [[RVLocationManager alloc] init];
            _locationManager.threshold = 0;
            _locationManager.delegate = self;
        }
        
        _visitManager.delegate = self;
        
        if (config.serverURL) {
            RVNetworkingManager *networkingManager = [RVNetworkingManager sharedManager];
            networkingManager.baseURL = [NSURL URLWithString:config.serverURL];
        } else {
            RV_LWARN(@"Empty server URL");
        }
        
        if ([config.applicationToken length]) {
            [[RVNetworkingManager sharedManager] setAuthToken:config.applicationToken];
        } else {
            RV_LERR(@"Empty `applicationToken`");
        }
        
        if (!config.gimbalMode && [config.beaconUUIDs count]) {
            [_visitManager.regionManager setBeaconUUIDs:config.beaconUUIDs];
        } else if (!config.gimbalMode) {
            RV_LWARN(@"Empty beacon UUIDS");
        }
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods

- (id)configValueForKey:(NSString *)key {
    return [_config valueForKey:key];
}

- (void)startMonitoring {
    if (!_config.gimbalMode) {
        [_visitManager.regionManager startMonitoring];
        [_visitManager.geofenceManager startMonitoring];
        [_locationManager startMonitoringLocationUpdates];
    } else {
        RV_LWARN(@"Use [GMBLPlaceManager startMonitoring] when in Gimbal mode.");
    }
}

- (void)stopMonitoring {
    if (!_config.gimbalMode) {
        [_visitManager.regionManager stopMonitoring];
        [_visitManager.geofenceManager stopMonitoring];
        [_locationManager stopMonitoringLocationUpdates];
    } else {
        RV_LWARN(@"Use [GMBLPlaceManager stopMonitoring] when in Gimbal mode.");
    }
}

- (void)simulateBeaconWithUUID:(NSUUID *)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor duration:(NSTimeInterval)duration
{
    if (_config.gimbalMode) {
        RV_LWARN(@"Cannot simulate beacons when in Gimbal model");
        return;
    }
    
    [_visitManager.regionManager simulateRegionEnterWithBeaconUUID:UUID major:major minor:minor];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_visitManager.regionManager simulateRegionExitWithBeaconUUID:UUID major:major minor:minor];
    });
}

- (void)simulateBeaconWithUUID:(NSUUID *)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor {
    [self simulateBeaconWithUUID:UUID major:major minor:minor duration:30];
}

#pragma mark - Utility

- (void)presentModalWithDecks:(NSArray *)decks {
    
    if ([decks filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RVDeck *deck, NSDictionary *bindings) {
        return deck.cards.count > 0;
    }]].count == 0) {
        RV_LWARN(@"presentModalWithDecks called but there are no cards to display");
        return;
    }
    
    _modalViewController = [[self.config.modalViewControllerClass alloc] init];
    
    RV_LTRACE(@"Will display cards modal...");
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverWillDisplayModalViewController:)]) {
        [self.delegate roverWillDisplayModalViewController:_modalViewController];
    }
    
    if ([_modalViewController isKindOfClass:[RXVisitViewController class]]) {
        ((RXVisitViewController *)_modalViewController).delegate = self;
        
        [((RXVisitViewController *)_modalViewController) setDecks:[[decks filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RVDeck *deck, NSDictionary *bindings) {
            return deck.cards.count > 0;
        }]] mutableCopy]];
    }
    
    if ([_modalViewController isKindOfClass:[RXModalViewController class]]) {
        [((RXModalViewController *)_modalViewController) setBackdropBlurRadius:self.config.modalBackdropBlurRadius];
        [((RXModalViewController *)_modalViewController) setBackdropTintColor:self.config.modalBackdropTintColor];
    }
    
    CGRect frame = [UIScreen mainScreen].bounds;
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        frame = CGRectMake(0, 0, frame.size.height, frame.size.width);
    }
    
    // safety net in case our window becomes key for whatever reason
    _applicationKeyWindow = [UIApplication sharedApplication].keyWindow;
    
    _window = [[UIWindow alloc] initWithFrame:frame];
    _window.hidden = NO;
    [_window setRootViewController:[RXFixedViewController new]];
    [_window.rootViewController presentViewController:_modalViewController animated:YES completion:nil];
    
    RV_LTRACE(@"Did display modal.");
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverDidDisplayModalViewController:)]) {
        [self.delegate roverDidDisplayModalViewController:_modalViewController];
    }
}

- (void)presentLocalNotification:(NSString *)message userInfo:(NSDictionary *)userInfo {
    UILocalNotification *note = [[UILocalNotification alloc] init];
    note.alertBody = message;
    
    NSMutableDictionary *userInfoDict = [NSMutableDictionary dictionaryWithDictionary:userInfo];
    [userInfoDict setValue:@YES forKey:@"_rover"];
    note.userInfo = [NSDictionary dictionaryWithDictionary:userInfoDict];
    
    if (self.config.notificationSoundName) {
        note.soundName = self.config.notificationSoundName;
    }
    [[UIApplication sharedApplication] presentLocalNotificationNow:note];
    
    RV_LDEBUG(@"Presented local notification: %@", note);
}

- (void)trackEvent:(NSString *)event params:(NSDictionary *)params {
    if (self.currentVisit) {
        [[RVNetworkingManager sharedManager] trackEvent:event params:params visit:self.currentVisit];
    }
}

#pragma mark - RVVisitManagerDelegate

- (void)visitManager:(RVVisitManager *)manager didPotentiallyExitLocation:(RVLocation *)location visit:(RVVisit *)visit {
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverVisit:didPotentiallyExitLocation:aliveForAnother:)]) {
        [self.delegate roverVisit:visit didPotentiallyExitLocation:visit.location aliveForAnother:visit.keepAlive];
    }
}

- (void)visitManager:(RVVisitManager *)manager didExpireVisit:(RVVisit *)visit {
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverVisitDidExpire:)]) {
        [self.delegate roverVisitDidExpire:visit];
    }
    
    [[RVAssetManager sharedManager] clearCache];
}

- (void)visitManager:(RVVisitManager *)manager didEnterTouchpoint:(RVTouchpoint *)touchpoint visit:(RVVisit *)visit {

    // Touchpoint Tracking
    [self trackEvent:@"touchpoint.enter" params:@{@"touchpoint": touchpoint.ID}];
    
    // TODO: get better with card tracking and using the `delivered` flag on RVDeck
    // Card Delivered Tracking
    RVDeck *deck = [visit deckWithID:touchpoint.deckId];
    if (deck) {
        for (RVCard *card in deck.cards) {
            [self trackEvent:@"card.deliver" params:@{@"card": card.ID}];
        }
    }
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverVisit:didEnterTouchpoint:)]) {
        [self.delegate roverVisit:visit didEnterTouchpoint:touchpoint];
    }
}

- (void)visitManager:(RVVisitManager *)manager didExitTouchpoint:(RVTouchpoint *)touchpoint visit:(RVVisit *)visit {
    
    // Touchpoint tracking
    [self trackEvent:@"touchpoint.exit" params:@{@"touchpoint": touchpoint.ID}];
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverVisit:didExitTouchpoint:)]) {
        [self.delegate roverVisit:visit didExitTouchpoint:touchpoint];
    }
}

- (BOOL)visitManager:(RVVisitManager *)manager shouldCreateVisit:(RVVisit *)visit {
    visit.simulate = self.config.sandboxMode;
    
    // Delegate
    if ([self.delegate respondsToSelector:@selector(roverShouldCreateVisit:)]) {
        if (![self.delegate roverShouldCreateVisit:visit]) {
            RV_LDEBUG(@"Visit creation was prevented");
            return NO;
        }
    }
    
    [[RVNetworkingManager sharedManager] postVisit:visit];
    
    if (visit.ID) {
        
        // TODO: this is temp and should be moved to the networking layer in the next maintenance release
        [visit mapRelations];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[RVAssetPrefetcher sharedAssetPrefetcher] prefetchURLs:visit.allImageUrls];
        });
        
        // Delegate
        if ([self.delegate respondsToSelector:@selector(roverDidCreateVisit:)]) {
            [self.delegate roverDidCreateVisit:visit];
        }
        
        return YES;
    }
    
    return NO;
}

#pragma mark - RVLocationManagerDelegate

- (void)locationManager:(RVLocationManager *)manager didChangeLocation:(CLLocation *)location {
    if (self.currentVisit) {
        return;
    }
    
    RV_LTRACE(@"Updating list of monitored geofences...");
    
    [[RVNetworkingManager sharedManager] getGeofencesNearLatitude:location.coordinate.latitude longitude:location.coordinate.longitude success:^(NSArray *geofences) {
        NSMutableArray *geofenceRegions = [NSMutableArray array];
        for (RVGeofence *geofence in geofences) {
            [geofenceRegions addObject:[geofence CLCircularRegion]];
        }
        
        [_visitManager.geofenceManager setGeofenceRegions:[NSOrderedSet orderedSetWithArray:geofenceRegions]];
        
        if (_visitManager.geofenceManager.isMonitoring) {
            [_visitManager.geofenceManager stopMonitoring];
            [_visitManager.geofenceManager startMonitoring];
        }
        
    } failure:nil];
}

#pragma mark - Application Notifications

- (void)applicationDidFinishLaunching:(NSNotification *)note {
    // This async is needed because a UIWindow without a rootViewController may be added
    // at this point. There is no error, but this is just to silence the warning and also
    // differ any experience logic till after the launch
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didOpenApplication];
        
        UILocalNotification *localNotification = [note.userInfo objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        if (localNotification) {
            [self handleDidReceiveLocalNotification:localNotification];
        }
    });
}

- (void)applicationWillEnterForeground:(NSNotification *)note {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self didOpenApplication];
    });
}

- (void)didOpenApplication {
    if (self.currentVisit) {
        
        RV_LDEBUG(@"Application opened during visit: %@", self.currentVisit);
        
        // Delegate
        if ([self.delegate respondsToSelector:@selector(didOpenApplicationDuringVisit:)]) {
            [self.delegate didOpenApplicationDuringVisit:self.currentVisit];
        }
    }
}

- (BOOL)handleDidReceiveLocalNotification:(UILocalNotification *)notification {
    RV_LTRACE(@"Notification swiped");
    
    if ([notification.userInfo objectForKey:@"_rover"] && self.currentVisit) {
        if ([self.delegate respondsToSelector:@selector(didReceiveRoverNotificationWithUserInfo:)]) {
            [self.delegate didReceiveRoverNotificationWithUserInfo:notification.userInfo];
        }
        return YES;
    }
    return NO;
}

#pragma mark - RXVisitViewControllerDelegate

- (void)visitViewController:(RXVisitViewController *)viewController didDisplayCard:(RVCard *)card {
    [self trackEvent:@"card.view" params:@{@"card": card.ID}];
    
    if ([self.delegate respondsToSelector:@selector(roverVisit:didDisplayCard:)]) {
        [self.delegate roverVisit:self.currentVisit didDisplayCard:card];
    }
}

- (void)visitViewController:(RXVisitViewController *)viewController didDiscardCard:(RVCard *)card {
    [self trackEvent:@"card.discard" params:@{@"card": card.ID}];
    
    if ([self.delegate respondsToSelector:@selector(roverVisit:didDiscardCard:)]) {
        [self.delegate roverVisit:self.currentVisit didDiscardCard:card];
    }
}

- (void)visitViewController:(RXVisitViewController *)viewController didClickCard:(RVCard *)card URL:(NSURL *)url {
    [self trackEvent:@"card.click" params:@{@"card": card.ID, @"url": url.absoluteString}];
    
    if ([self.delegate respondsToSelector:@selector(roverVisit:didClickCard:withURL:)]) {
        [self.delegate roverVisit:self.currentVisit didClickCard:card withURL:url];
    }
}

- (void)visitViewControllerDidGetDismissed:(RXVisitViewController *)viewController {
    _modalViewController = nil;
    _window.rootViewController = nil;
    _window = nil;
    
    if ([self.delegate respondsToSelector:@selector(roverDidDismissModalViewController)]) {
        [self.delegate roverDidDismissModalViewController];
    }
}

- (void)visitViewControllerWillGetDismissed:(RXVisitViewController *)viewController {
    [_applicationKeyWindow makeKeyWindow];
    _applicationKeyWindow = nil;
    if ([self.delegate respondsToSelector:@selector(roverWillDismissModalViewController:)]) {
        [self.delegate roverWillDismissModalViewController:viewController];
    }
}

@end
