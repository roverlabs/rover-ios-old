//
//  RVGimbalVisitManager.h
//  Pods
//
//  Created by Ata Namvari on 2015-11-03.
//
//

#import <Foundation/Foundation.h>
#import "RVVisitManager.h"

@class GMBLPlaceManager;

/** This class does the heavy lifting of managing a journey through Gimbal places.
 */
@interface RVGimbalVisitManager : NSObject

/** Delegate that gets notified of location and touchpoint events.
 */
@property (nonatomic, weak) NSObject <RVVisitManagerDelegate> *delegate;

/** The Gimbal place manager that is responsible for monitoring and ranging for beacons and geofences.
 */
@property (strong, nonatomic, readonly) GMBLPlaceManager *placeManager;

/** The latest visit object seen by the visit manager.
 */
@property (strong, nonatomic, readonly) RVVisit *latestVisit;

@end
