//
//  RVGeofenceManager.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-14.
//
//

#import "RVGeofenceManager.h"

#import "RVGlobal.h"

NSString *const RVGeofenceManagerCurrentRegionsKey = @"RVGeofenceManagerCurrentRegionsKey";

@interface RVGeofenceManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) NSSet *currentRegions;

@end

@implementation RVGeofenceManager {
    NSSet *_currentRegions;
    BOOL _monitoringStarted;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    return self;
}

#pragma mark - Properties

- (NSSet *)currentRegions {
    if (_currentRegions) {
        return _currentRegions;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:RVGeofenceManagerCurrentRegionsKey];
    if (data) {
        _currentRegions = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        RV_LTRACE(@"Pulling current geofence regions from disk: %@", _currentRegions);
    }
    if (!_currentRegions)
        _currentRegions = [NSSet set];
    
    return _currentRegions;
}

- (void)setCurrentRegions:(NSSet *)currentRegions {
    _currentRegions = currentRegions;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentRegions];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:RVGeofenceManagerCurrentRegionsKey];
}

- (BOOL)isMonitoring {
    return _monitoringStarted;
}

#pragma mark - Instance Methods

- (void)startMonitoring {
    if (CLLocationManager.authorizationStatus != kCLAuthorizationStatusAuthorizedAlways && [_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    
    for (CLCircularRegion *region in self.geofenceRegions) {
        [_locationManager startMonitoringForRegion:region];
    }
    
    // NOTE: This is cause of an Apple Bug
    // https://www.cocoanetics.com/2014/05/radar-monitoring-clregion-immediately-after-removing-one-fails/
    
    // TODO: This has got to be more precise
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (CLCircularRegion *region in self.geofenceRegions) {
            [_locationManager requestStateForRegion:region];
        }
    });
    
    _monitoringStarted = YES;
    
    RV_LDEBUG(@"Geofence monitoring started for regions: %@", self.geofenceRegions);
}

- (void)stopMonitoring {
    for (CLRegion *region in _locationManager.monitoredRegions) {
        if ([region isKindOfClass:[CLCircularRegion class]]) {
            [_locationManager stopMonitoringForRegion:region];
        }
    }
    
    RV_LDEBUG(@"Geofence monitoring stopped");
    
    _monitoringStarted = NO;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [self handleEnterForRegion:(CLCircularRegion *)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        [self handleExitForRegion:(CLCircularRegion *)region];
    }
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLCircularRegion class]]) {
        switch (state) {
            case CLRegionStateInside:
                [self handleEnterForRegion:(CLCircularRegion *)region];
                break;
            case CLRegionStateOutside:
                [self handleExitForRegion:(CLCircularRegion *)region];
                break;
            default:
                break;
        }
    }
    
    RV_LTRACE(@"State determined (%ld) for geofence region: %@", (long)state, region);
}

// TODO: do the same thing in RVRegionManager

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (manager == _locationManager && status == kCLAuthorizationStatusAuthorizedAlways && _monitoringStarted) {
        RV_LTRACE(@"Location manager always authorization granted");
        [self startMonitoring];
    } else {
        RV_LDEBUG(@"Location manager always authorization revoked");
    }
}

#pragma mark - Helpers

- (void)handleExitForRegion:(CLCircularRegion *)region {
    if ([self.currentRegions containsObject:region]) {
        NSMutableSet *regions = [NSMutableSet setWithSet:self.currentRegions];
        [regions removeObject:region];
        self.currentRegions = [NSSet setWithSet:regions];
        
        [self.delegate geofenceManager:self didExitRegion:(CLCircularRegion *)region];
        RV_LTRACE(@"Exited geofence region: %@", region);
    }
}

- (void)handleEnterForRegion:(CLCircularRegion *)region {
    if (![self.currentRegions containsObject:region]) {
        NSMutableSet *regions = [NSMutableSet setWithSet:self.currentRegions];
        [regions addObject:region];
        self.currentRegions = [NSSet setWithSet:regions];
        
        [self.delegate geofenceManager:self didEnterRegion:(CLCircularRegion *)region];
        RV_LTRACE(@"Entered geofence region: %@", region);
    }
}

@end
