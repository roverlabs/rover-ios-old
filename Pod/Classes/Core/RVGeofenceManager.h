//
//  RVGeofenceManager.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-14.
//
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@protocol RVGeofenceManagerDelegate;

/** This class is responsible for monitoring for geofences.
 */
@interface RVGeofenceManager : NSObject


/** Delegate that gets notified of geofence events.
 */
@property (nonatomic, weak) id<RVGeofenceManagerDelegate> delegate;

/** An NSSet of CLCircularRegions the user is currently in.
 */
@property (nonatomic, readonly) NSSet *currentRegions;

/** The NSOrderedSet of CLCircularRegions that should be monitored.
 */
@property (nonatomic, strong) NSOrderedSet *geofenceRegions;

/** Indicates if manager is currently monitoring for geofence enters and exits.
 */
@property (nonatomic, readonly) BOOL isMonitoring;

/** Start monitoring for geofence events.
*/
- (void)startMonitoring;

/** Stop monitoring for geofence events.
 */
- (void)stopMonitoring;

@end


@protocol RVGeofenceManagerDelegate <NSObject>

/** Called when the user enters a geofence.
 
 @param manager The geofence manager instance thats calling the delegate method.
 @param region The CLCircularRegion that the user entered.
 */
- (void)geofenceManager:(RVGeofenceManager *)manager didEnterRegion:(CLCircularRegion *)region;

/** Called when the user exits a geofence.
 
 @param manager The geofence manager instance thats calling the delegate method.
 @param region The CLCircularRegion that the user exited.
 */
- (void)geofenceManager:(RVGeofenceManager *)manager didExitRegion:(CLCircularRegion *)region;

@end
