//
//  RVVisitManager.m
//  Rover
//
//  Created by Sean Rucker on 2014-07-29.
//  Copyright (c) 2014 Rover Labs Inc. All rights reserved.
//

#import "RVVisitManager.h"

#import "RVGlobal.h"

#import "RVRegionManager.h"
#import "RVGeofenceManager.h"
#import "RVTimerController.h"

#import "RVCard.h"
#import "RVCustomer.h"
#import "RVVisit.h"
#import "RVTouchpoint.h"
#import "RVBeaconRegion.h"
#import "RVCustomer.h"

NSString *const RVVisitManagerExpirationTimerIdentifier = @"RVVisitManagerExpirationTimerIdentifier";

void rv_runOnMainQueue(void (^block)()) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

@interface RVVisitManager () <RVRegionManagerDelegate, RVGeofenceManagerDelegate, RVTimerControllerDelegate>

@property (strong, nonatomic) RVVisit *latestVisit;
@property (strong, nonatomic) RVTimerController *timerController;
@property (strong, nonatomic) NSOperationQueue *operationQueue;

@end

@implementation RVVisitManager

#pragma mark - Initialization

- (id)init {
    self = [super init];
    if (self) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
        
        _regionManager = [RVRegionManager new];
        _regionManager.delegate = self;
        
        _geofenceManager = [RVGeofenceManager new];
        _geofenceManager.delegate = self;
        
        _timerController = [RVTimerController new];
        _timerController.delegate = self;
    }
    return  self;
}

#pragma mark - Instance methods

- (RVVisit *)latestVisit
{
    if (_latestVisit) {
        return _latestVisit;
    }
    
    _latestVisit = [RVVisit latestVisit];
    
    for (CLBeaconRegion *region in _regionManager.currentRegions) {
        NSArray *touchpoints = [_latestVisit touchpointsForRegion:region];
        for (RVTouchpoint *touchpoint in touchpoints) {
            [_latestVisit addToCurrentTouchpoints:touchpoint];
        }
    }
    
    for (CLCircularRegion *region in _geofenceManager.currentRegions) {
        NSArray *touchpoints = [_latestVisit touchpointsForRegion:region];
        for (RVTouchpoint *touchpoint in touchpoints) {
            [_latestVisit addToCurrentTouchpoints:touchpoint];
        }
    }
    
    return _latestVisit;
}

- (void)didEnterRegion:(CLRegion *)region {
    CLRegion *cRegion = [region copy];
    [_operationQueue addOperationWithBlock:^{
        BOOL isBeaconRegion = [cRegion isKindOfClass:[CLBeaconRegion class]];
        RVVisit *latestVisit = self.latestVisit;
        
        if (latestVisit.isAlive && [latestVisit respondsToRegion:cRegion]) {
            RV_LTRACE(@"EnteredRegion corresponds to current visit: %@", cRegion);
            
            NSArray *touchpoints = [latestVisit touchpointsForRegion:cRegion];
            NSArray *newTouchpoints = [touchpoints filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(RVTouchpoint *touchpoint, NSDictionary<NSString *,id> * _Nullable bindings) {
                return ![latestVisit.currentTouchpoints containsObject:touchpoint];
            }]];
            
            [_timerController invalidateTimerWithIdentifier:RVVisitManagerExpirationTimerIdentifier];
            
            [self movedToTouchpoints:newTouchpoints];
            
            [RVVisit setLatestVisit:self.latestVisit];
            
            if (isBeaconRegion && _regionManager.monitoredRegions.count == _regionManager.beaconUUIDs.count) {
                [self startMonitoringForVisit:latestVisit];
            }
            
            return;
        }
        
        RVVisit *newVisit = [self visitWithRegion:cRegion];
        if (newVisit) {
            if ([_timerController fireDateForTimerWithIdentifier:RVVisitManagerExpirationTimerIdentifier] != nil || self.latestVisit.isAlive) {
                [_timerController invalidateTimerWithIdentifier:RVVisitManagerExpirationTimerIdentifier];
                [self expireVisit];
            }
            
            self.latestVisit = newVisit;
            
            NSArray *touchpoints = [newVisit touchpointsForRegion:region];
            [self movedToTouchpoints:touchpoints];
            
            [RVVisit setLatestVisit:newVisit];
            
            if (isBeaconRegion)
                [self startMonitoringForVisit:newVisit];
        }
    }];
}

- (void)didExitRegion:(CLRegion *)region {
    CLRegion *cRegion = [region copy];
    [_operationQueue addOperationWithBlock:^{
        BOOL isBeaconRegion = [cRegion isKindOfClass:[CLBeaconRegion class]];
        RVVisit *latestVisit = self.latestVisit;
        
        if (latestVisit && [latestVisit respondsToRegion:cRegion]) {
            RV_LTRACE(@"ExitedRegion corresponds to current visit: %@", cRegion);
            
            NSArray *touchpoints = [latestVisit touchpointsForRegion:cRegion];
            for (RVTouchpoint *touchpoint in touchpoints) {
                
                if (![self isStillInTouchpoint:touchpoint visit:latestVisit]) {
                    [latestVisit removeFromCurrentTouchpoints:touchpoint];
                    
                    RV_LDEBUG(@"Exited touchpoint: %@", touchpoint);
                    
                    // Delegate
                    if ([self.delegate respondsToSelector:@selector(visitManager:didExitTouchpoint:visit:)]) {
                        rv_runOnMainQueue(^{
                            [self.delegate visitManager:self didExitTouchpoint:touchpoint visit:self.latestVisit];
                        });
                    }
                    
                    [RVVisit setLatestVisit:latestVisit];
                }
            }
            
            if (isBeaconRegion) {
                if (![self isVisitStillInRangeOfBeacons:latestVisit]) {
                    RV_LTRACE(@"No longer in range of beacons");
                    
                    latestVisit.beaconLastDetectedAt = [NSDate date];
                    [RVVisit setLatestVisit:latestVisit];
                    
                    if (latestVisit.isBeaconTriggered) {
                        rv_runOnMainQueue(^{
                            [_timerController scheduleTimerWithInterval:latestVisit.keepAlive identifier:RVVisitManagerExpirationTimerIdentifier];
                        });
                    }
                    
                    RV_LDEBUG(@"Potentially exited location: %@", self.latestVisit.location);
                    
                    // Delegate
                    if ([self.delegate respondsToSelector:@selector(visitManager:didPotentiallyExitLocation:visit:)]) {
                        rv_runOnMainQueue(^{
                            [self.delegate visitManager:self didPotentiallyExitLocation:self.latestVisit.location visit:self.latestVisit];
                        });
                    }
                    
                    // stop monitoring for beacons
                    [self stopMonitoringForVisit];
                }
                
            } else if (latestVisit.currentTouchpoints.count == 0) {
                
                RV_LTRACE(@"No longer in any touchpoint");
                //                // Delegate
                //                if ([self.delegate respondsToSelector:@selector(visitManager:didPotentiallyExitLocation:visit:)]) {
                //                    [self executeOnMainQueue:^{
                //                        [self.delegate visitManager:self didPotentiallyExitLocation:self.latestVisit.location visit:self.latestVisit];
                //                    }];
                //                }
                [self expireVisit];
            }
            
        }
    }];
}

#pragma mark - RVRegionManagerDelegate

- (void)regionManager:(RVRegionManager *)manager didEnterRegion:(CLBeaconRegion *)region {
    [self didEnterRegion:region];
}

- (void)regionManager:(RVRegionManager *)manager didExitRegion:(CLBeaconRegion *)region {
    [self didExitRegion:region];
}

#pragma mark - RVGeofenceManagerDelegate

- (void)geofenceManager:(RVGeofenceManager *)manager didEnterRegion:(CLCircularRegion *)region {
    [self didEnterRegion:region];
}

- (void)geofenceManager:(RVGeofenceManager *)manager didExitRegion:(CLCircularRegion *)region {
    [self didExitRegion:region];
}

#pragma mark - Visit Creation

- (RVVisit *)visitWithRegion:(CLRegion *)region {
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        return [self visitWithBeaconRegion:(CLBeaconRegion *)region];
    } else {
        return [self visitWithTouchpointIdentifier:region.identifier];
    }
}

- (RVVisit *)visitWithBeaconRegion:(CLBeaconRegion *)region {
    RVVisit *newVisit = [self newVisit];
    newVisit.beaconRegion = [RVBeaconRegion beaconRegionWithCLBeaconRegion:region];
    
    if ([self shouldCreateVisit:newVisit]) {
        RV_LDEBUG(@"Visit created with beacon region: %@", newVisit);
        return newVisit;
    }
    
    return nil;
}

- (RVVisit *)visitWithTouchpointIdentifier:(NSString *)identifier {
    RVVisit *newVisit = [self newVisit];
    newVisit.touchpointIdentifier = identifier;
    
    if ([self shouldCreateVisit:newVisit]) {
        RV_LDEBUG(@"Visit created with touchpointIdentifier: %@", newVisit);
        return newVisit;
    }
    
    return nil;
}

- (RVVisit *)newVisit {
    RVVisit *newVisit = [RVVisit new];
    newVisit.customer = [RVCustomer cachedCustomer];
    newVisit.timestamp = [NSDate date];
    return newVisit;
}

- (BOOL)shouldCreateVisit:(RVVisit *)visit {
    if ([self.delegate respondsToSelector:@selector(visitManager:shouldCreateVisit:)]) {
        return [self.delegate visitManager:self shouldCreateVisit:visit];
    }
    return YES;
}

#pragma mark - RVTimerControllerDelegate

- (void)timerController:(RVTimerController *)controller didFireTimerWithIdentifier:(id<NSCopying>)identifier {
    if ([(NSString *)identifier isEqualToString:RVVisitManagerExpirationTimerIdentifier]) {
        [self expireVisit];
    }
}

#pragma mark - Helper Methods

- (BOOL)isStillInTouchpoint:(RVTouchpoint *)touchpoint visit:(RVVisit *)visit {
    for (CLBeaconRegion *beaconRegion in _regionManager.currentRegions) {
        if ([touchpoint respondsToRegion:beaconRegion]) {
            return YES;
        }
    }
    for (CLRegion *circularRegion in _geofenceManager.currentRegions) {
        if ([touchpoint respondsToRegion:circularRegion]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isVisitStillInRangeOfBeacons:(RVVisit *)visit {
    for (RVTouchpoint *touchpoint in visit.currentTouchpoints) {
        if (touchpoint.beaconRegions.count > 0) {
            return YES;
        }
    }
    return NO;
}

- (void)movedToTouchpoints:(NSArray *)touchpoints {
    for (RVTouchpoint *touchpoint in touchpoints) {
        [self.latestVisit addToCurrentTouchpoints:touchpoint];
        
        RV_LDEBUG(@"Entered touchpoint: %@", touchpoint);
        
        // Delegate
        if ([self.delegate respondsToSelector:@selector(visitManager:didEnterTouchpoint:visit:)]) {
            rv_runOnMainQueue(^{
                [self.delegate visitManager:self didEnterTouchpoint:touchpoint visit:self.latestVisit];
            });
        }
    }
}

- (void)startMonitoringForVisit:(RVVisit *)visit {
    [_geofenceManager stopMonitoring];
    NSArray *regions = [visit.observableRegions.array subarrayWithRange:NSMakeRange(0, MIN(visit.observableRegions.count, (visit.beaconRegion ? 19 : 18)))];
    [_regionManager startMonitoringForRegions:regions];
}

- (void)stopMonitoringForVisit {
    [_regionManager stopMonitoringForAllSpecificRegions];
    [_geofenceManager startMonitoring];
}

- (void)expireVisit {
    RVVisit *visit = self.latestVisit;
    _latestVisit = nil;
    [RVVisit clearLatestVisit];
    
    RV_LDEBUG(@"Visit expired: %@", visit);
    
    if ([self.delegate respondsToSelector:@selector(visitManager:didExpireVisit:)]) {
        rv_runOnMainQueue(^{
            [self.delegate visitManager:self didExpireVisit:visit];
        });
    }
}



@end