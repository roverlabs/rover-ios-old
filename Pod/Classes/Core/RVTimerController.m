//
//  RVTimerController.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-23.
//
//

#import "RVTimerController.h"
#import "RVGlobal.h"

@import UIKit;

NSString *const RVTimerControllerSchedulesDictionaryKey = @"RVTimerControllerSchedulesDictionaryKey";
NSString *const RVTimerControllerIdentifierKey = @"RVTimerControllerIdentifierKey";

@interface RVTimerController ()

@property (nonatomic, strong) NSMutableDictionary *schedulesDictionary;
@property (nonatomic, strong) NSMutableDictionary *timersDictionary;

@end

@implementation RVTimerController

- (instancetype)init {
    self = [super init];
    if (self) {
        _timersDictionary = [NSMutableDictionary dictionary];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Properties

- (NSMutableDictionary *)schedulesDictionary {
    if (_schedulesDictionary) {
        return _schedulesDictionary;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:RVTimerControllerSchedulesDictionaryKey];
    if (data) {
        _schedulesDictionary = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        RV_LTRACE(@"Pulling timer schedules dictionary from disk: %@", _schedulesDictionary);
    }
    
    if (!_schedulesDictionary) {
        _schedulesDictionary = [NSMutableDictionary dictionary];
    }
    
    return _schedulesDictionary;
}

#pragma mark - Instance Methods

- (void)scheduleTimerWithInterval:(NSTimeInterval)interval identifier:(id<NSCopying>)identifier {
    RV_LDEBUG(@"Creating timer for `%f` seconds: %@", interval, identifier);
    
    NSDate *scheduledDate = [[NSDate date] dateByAddingTimeInterval:interval];
    [self.schedulesDictionary setObject:scheduledDate forKey:identifier];
    
    [self persistSchedulesToDisk];
    
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground) {
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(timerFired:) userInfo:@{RVTimerControllerIdentifierKey: identifier} repeats:NO];
        [self.timersDictionary setObject:timer forKey:identifier];
        RV_LTRACE(@"Timer started: %@", identifier);
    }
}

- (void)invalidateTimerWithIdentifier:(id<NSCopying>)identifier {
    NSTimer *timer = [self.timersDictionary objectForKey:identifier];
    [timer invalidate];
    
    [self.timersDictionary removeObjectForKey:identifier];
    [self.schedulesDictionary removeObjectForKey:identifier];
    
    [self persistSchedulesToDisk];
    
    RV_LDEBUG(@"Timer invalidated: %@", identifier);
}

- (NSDate *)fireDateForTimerWithIdentifier:(id<NSCopying>)identifier {
    return [self.schedulesDictionary objectForKey:identifier];
}

- (NSTimer *)timerWithIdentifier:(id<NSCopying>)identifier {
    return [self.timersDictionary objectForKey:identifier];
}

#pragma mark - Private Methods

- (void)timerFired:(NSTimer *)timer {
    NSDictionary *userInfo = [timer userInfo];
    id<NSCopying> identifier = [userInfo objectForKey:RVTimerControllerIdentifierKey];
    
    [self.timersDictionary removeObjectForKey:identifier];
    [self.schedulesDictionary removeObjectForKey:identifier];
    
    [self persistSchedulesToDisk];
    
    RV_LDEBUG(@"Timer fired: %@", identifier);
    
    [self.delegate timerController:self didFireTimerWithIdentifier:identifier];
}

- (void)persistSchedulesToDisk {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.schedulesDictionary];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:RVTimerControllerSchedulesDictionaryKey];
}

#pragma mark - UIApplicationStateNotification

- (void)applicationDidEnterBackground:(NSNotification *)note {
    for (id<NSCopying> identifier in self.timersDictionary) {
        NSTimer *timer = [self.timersDictionary objectForKey:identifier];
        [timer invalidate];
        
        [self.timersDictionary removeObjectForKey:identifier];
    }
}

- (void)applicationWillEnterForeground:(NSNotification *)note {
    [self applicationDidOpen];
}

- (void)applicationDidFinishLaunching:(NSNotification *)note {
    [self applicationDidOpen];
}

- (void)applicationDidOpen {
    for (id<NSCopying> identifier in self.schedulesDictionary) {
        NSDate *fireDate = [self.schedulesDictionary objectForKey:identifier];
        NSTimeInterval remainingInterval = MAX(2, [fireDate timeIntervalSinceDate:[NSDate date]]);
        NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:remainingInterval target:self selector:@selector(timerFired:) userInfo:@{RVTimerControllerIdentifierKey: identifier} repeats:NO];
        
        RV_LTRACE(@"Timer started for `%f` seconds: %@", remainingInterval, identifier);
        
        [self.timersDictionary setObject:timer forKey:identifier];
    }
}

@end
