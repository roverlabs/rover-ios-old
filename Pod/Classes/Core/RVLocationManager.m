//
//  RVLocationManager.m
//  Pods
//
//  Created by Ata Namvari on 2015-10-21.
//
//

#import "RVLocationManager.h"

#import "RVGlobal.h"

NSString *const RVLocationManagerLastUpdatedLocationKey = @"RVLocationManagerLastUpdatedLocationKey";

@interface RVLocationManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation RVLocationManager {
    BOOL _monitoringStarted;
    CLLocation *_lastUpdatedLocation;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _threshold = 0;
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    return self;
}

#pragma mark - Properties

- (CLLocation *)lastUpdatedLocation {
    if (_lastUpdatedLocation) {
        return _lastUpdatedLocation;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:RVLocationManagerLastUpdatedLocationKey];
    if (data) {
        _lastUpdatedLocation = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        RV_LTRACE(@"Pulling last updated location from disk: %@", _lastUpdatedLocation);
    }
    
    return _lastUpdatedLocation;
    
}

- (void)setLastUpdatedLocation:(CLLocation *)lastUpdatedLocation {
    _lastUpdatedLocation = lastUpdatedLocation;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:lastUpdatedLocation];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:RVLocationManagerLastUpdatedLocationKey];
}

#pragma mark - Instance Methods

- (void)startMonitoringLocationUpdates {
    if (CLLocationManager.authorizationStatus != kCLAuthorizationStatusAuthorizedAlways && [_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    
    [_locationManager startMonitoringSignificantLocationChanges];
    _monitoringStarted = YES;
    
    RV_LDEBUG(@"Location updates monitoring started");
}

- (void)stopMonitoringLocationUpdates {
    [_locationManager stopMonitoringSignificantLocationChanges];
    _monitoringStarted = NO;
    
    RV_LDEBUG(@"Location updates monitoring stopped");
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (manager == _locationManager && status == kCLAuthorizationStatusAuthorizedAlways && _monitoringStarted) {
        [self startMonitoringLocationUpdates];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = locations.lastObject;
    
    RV_LTRACE(@"Location update received: %@", location);
    
    if (self.lastUpdatedLocation) {
        CLLocationDistance distance = [location distanceFromLocation:self.lastUpdatedLocation];
        if (distance < _threshold * 1000) {
            return;
        }
    }
    
    self.lastUpdatedLocation = location;
    
    RV_LDEBUG(@"Location manager did change location: %@", location);
    
    [self.delegate locationManager:self didChangeLocation:location];
}

@end
