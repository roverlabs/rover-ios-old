//
//  RVGimbalVisitManager.m
//  Pods
//
//  Created by Ata Namvari on 2015-11-03.
//
//

#import "RVGimbalVisitManager.h"

#import "RVGlobal.h"

#import "RVTouchpoint.h"
#import "RVVisit.h"
#import "RVCustomer.h"

@interface GMBLPlaceManager : NSObject
@property (weak, nonatomic) id delegate;
- (NSArray *)currentVisits;
@end

@interface GMBLPlace : NSObject
@property (nonatomic, readonly) NSString *identifier;
@end

@interface GMBLVisit : NSObject
@property (nonatomic, readonly) GMBLPlace *place;
@end

@interface RVGimbalVisitManager ()

@property (strong, nonatomic) RVVisit *latestVisit;
@property (strong, nonatomic) NSOperationQueue *operationQueue;

@end

@implementation RVGimbalVisitManager

#pragma mark - Initialization

- (id)init {
    self = [super init];
    if (self) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.maxConcurrentOperationCount = 1;
        
        Class gimbalPlaceManagerClass = NSClassFromString(@"GMBLPlaceManager");
        if (gimbalPlaceManagerClass) {
            _placeManager = [[gimbalPlaceManagerClass alloc] init];
            _placeManager.delegate = self;
        } else {
            NSLog(@"Gimbal SDK not found. Make sure you have linked against the Gimbal.framework");
            abort();
        }
    }
    return  self;
}

#pragma mark - Instance methods

- (RVVisit *)latestVisit
{
    if (_latestVisit) {
        return _latestVisit;
    }
    
    _latestVisit = [RVVisit latestVisit];
    
    for (GMBLVisit *gmblVisit in _placeManager.currentVisits) {
        GMBLPlace *place = gmblVisit.place;
        NSString *gimbalIdentifier = place.identifier;
        
        RVTouchpoint *touchpoint = [_latestVisit touchpointWithGimbalIdentifier:gimbalIdentifier];
        if (touchpoint) {
            [_latestVisit addToCurrentTouchpoints:touchpoint];
        }
    }
    
    return _latestVisit;
}

#pragma mark - GMBLPlaceManagerDelegate

- (void)placeManager:(GMBLPlaceManager *)manager didBeginVisit:(GMBLVisit *)visit {
    NSString *gimbalIdentifier = visit.place.identifier;
    
    [_operationQueue addOperationWithBlock:^{
        if (self.latestVisit && [self.latestVisit hasTouchpointWithGimbalIdentifier:gimbalIdentifier]) {
            RV_LTRACE(@"EnteredGimbalPlaceIdentifier corresponds to current visit: %@", gimbalIdentifier);
            
            [self movedToTouchpointWithGimbalIdentifier:gimbalIdentifier];
            
            [RVVisit setLatestVisit:self.latestVisit];
            
            return;
        }
        
        RVVisit *newVisit = [self visitWithGimbalPlaceIdentifier:gimbalIdentifier];
        if (newVisit) {
            if (self.latestVisit.isAlive) {
                [self expireVisit];
            }
            
            self.latestVisit = newVisit;
            
            [self movedToTouchpointWithGimbalIdentifier:gimbalIdentifier];
            
            [RVVisit setLatestVisit:newVisit];
        }
    }];
}

- (void)placeManager:(GMBLPlaceManager *)manager didEndVisit:(GMBLVisit *)visit {
    NSString *gimbalIdentifier = visit.place.identifier;
    
    [_operationQueue addOperationWithBlock:^{
        if (self.latestVisit && [self.latestVisit hasTouchpointWithGimbalIdentifier:gimbalIdentifier]) {
            RV_LTRACE(@"ExitedGimbalPlaceIdentifier corresponds to current visit: %@", gimbalIdentifier);
            
            RVTouchpoint *touchpoint = [self.latestVisit touchpointWithGimbalIdentifier:gimbalIdentifier];
            if (touchpoint) {
                [self.latestVisit removeFromCurrentTouchpoints:touchpoint];
                
                RV_LDEBUG(@"Exited touchpoint: %@", touchpoint);
                
                // Delegate
                if ([self.delegate respondsToSelector:@selector(visitManager:didExitTouchpoint:visit:)]) {
                    rv_runOnMainQueue(^{
                        [self.delegate visitManager:(RVVisitManager *)self didExitTouchpoint:touchpoint visit:self.latestVisit];
                    });
                }
                
                if (self.latestVisit.currentTouchpoints.count == 0) {
                    RV_LTRACE(@"No longer in any touchoint");
                    
                    self.latestVisit.beaconLastDetectedAt = [NSDate date];
                    
                    //                    if ([self.delegate respondsToSelector:@selector(visitManager:didPotentiallyExitLocation:visit:)]) {
                    //                        [self executeOnMainQueue:^{
                    //                            [self.delegate visitManager:self didPotentiallyExitLocation:self.latestVisit.location visit:self.latestVisit];
                    //                        }];
                    //                    }
                    
                    [self expireVisit];
                    
                } else {
                    [RVVisit setLatestVisit:self.latestVisit];
                }
            }
        }
    }];
}

#pragma mark - Visit Creation

- (RVVisit *)visitWithGimbalPlaceIdentifier:(NSString *)identifier {
    RVVisit *newVisit = [RVVisit new];
    newVisit.gimbalPlaceIdentifier = identifier;
    newVisit.customer = [RVCustomer cachedCustomer];
    newVisit.timestamp = [NSDate date];
    
    BOOL shouldCreateVisit = YES;
    if ([self.delegate respondsToSelector:@selector(visitManager:shouldCreateVisit:)]) {
        shouldCreateVisit = [self.delegate visitManager:(RVVisitManager *)self shouldCreateVisit:newVisit];
    }
    
    if (shouldCreateVisit) {
        RV_LDEBUG(@"Visit created with gimbalPlaceIdentifier: %@", newVisit);
        return newVisit;
    }
    
    return nil;
}

#pragma mark - Helper Methods

- (void)movedToTouchpointWithGimbalIdentifier:(NSString *)identifier {
    RVTouchpoint *touchpoint = [self.latestVisit touchpointWithGimbalIdentifier:identifier];
    if (touchpoint) {
        [self.latestVisit addToCurrentTouchpoints:touchpoint];
        
        RV_LDEBUG(@"Entered touchpoint: %@", touchpoint);
        
        // Delegate
        if ([self.delegate respondsToSelector:@selector(visitManager:didEnterTouchpoint:visit:)]) {
            rv_runOnMainQueue(^{
                [self.delegate visitManager:(RVVisitManager *)self didEnterTouchpoint:touchpoint visit:self.latestVisit];
            });
        }
    }
}


- (void)expireVisit {
    RVVisit *visit = self.latestVisit;
    _latestVisit = nil;
    [RVVisit clearLatestVisit];
    
    RV_LDEBUG(@"Visit expired: %@", visit);
    
    if ([self.delegate respondsToSelector:@selector(visitManager:didExpireVisit:)]) {
        rv_runOnMainQueue(^{
            [self.delegate visitManager:(RVVisitManager *)self didExpireVisit:visit];
        });
    }
}


@end
