//
//  RVLocationService.m
//  Rover
//
//  Created by Sean Rucker on 2014-06-23.
//  Copyright (c) 2014 Rover Labs Inc. All rights reserved.
//

#import "RVRegionManager.h"
#import "RVGlobal.h"

NSString *const RVRegionManagerCurrentRegionsKey = @"RVRegionManagerCurrentRegionsKey";

@interface RVRegionManager () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *beaconDetectedAt;
@property (nonatomic, strong) NSSet *currentRegions;

@end

@implementation RVRegionManager {
    NSSet *_currentRegions;
    BOOL _monitoringStarted;
}

#pragma mark - Instance Methods

// TODO: fix current Region stuff here

- (void)simulateRegionEnterWithBeaconUUID:(NSUUID *)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor {
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:UUID major:major minor:minor identifier:[self identifierForUUID:UUID major:major minor:minor]];
    
    NSMutableSet *tempCurrentRegions = [NSMutableSet setWithSet:self.currentRegions];
    [tempCurrentRegions addObject:beaconRegion];
    self.currentRegions = [NSSet setWithSet:tempCurrentRegions];
    
    [self.delegate regionManager:self didEnterRegion:beaconRegion];
}

- (void)simulateRegionExitWithBeaconUUID:(NSUUID *)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor {
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:UUID major:major minor:minor identifier:[self identifierForUUID:UUID major:major minor:minor]];
    
    NSMutableSet *tempCurrentRegions = [NSMutableSet setWithSet:self.currentRegions];
    [tempCurrentRegions removeObject:beaconRegion];
    self.currentRegions = [NSSet setWithSet:tempCurrentRegions];
    
    [self.delegate regionManager:self didExitRegion:beaconRegion];
}

#pragma mark - Properties

- (void)setBeaconUUIDs:(NSArray *)beaconUUIDs {
    _beaconUUIDs = beaconUUIDs;
    [self stopMonitoring];
    [self setupBeaconRegionsForUUIDs:beaconUUIDs];
}

- (void)setBeaconRegions:(NSMutableArray *)beaconRegions {
    _beaconRegions = beaconRegions;
    [self stopMonitoring];
    [self setupBeaconRegions];
}

- (NSSet *)currentRegions {
    if (_currentRegions) {
        return _currentRegions;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:RVRegionManagerCurrentRegionsKey];
    if (data) {
        _currentRegions = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        RV_LTRACE(@"Pulling current beacon regions from disk: %@", _currentRegions);
    }
    if (!_currentRegions)
        _currentRegions = [NSSet set];
    
    return _currentRegions;
}

- (void)setCurrentRegions:(NSSet *)currentRegions {
    _currentRegions = currentRegions;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:currentRegions];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:RVRegionManagerCurrentRegionsKey];
}

- (NSSet<CLBeaconRegion *> *)monitoredRegions {
    NSPredicate *beaconRegionPredicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return [evaluatedObject isKindOfClass:[CLBeaconRegion class]];
    }];
    return [_locationManager.monitoredRegions filteredSetUsingPredicate:beaconRegionPredicate];
}

#pragma mark - Initialization

- (id)init {
    self = [super init];
    if (self) {
        _beaconRegions = [[NSMutableArray alloc] initWithCapacity:20];
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
    }
    return  self;
}

// TODO: to save battery life, should only start ranging after monitoring has entered a location, then stop when exited the visit (keepalive?)

#pragma mark - Region monitoring

- (void)startMonitoring {
    if (CLLocationManager.authorizationStatus != kCLAuthorizationStatusAuthorizedAlways && [_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    
    [self.beaconRegions enumerateObjectsUsingBlock:^(CLBeaconRegion *beaconRegion, NSUInteger idx, BOOL *stop) {
        [self.locationManager startMonitoringForRegion:beaconRegion];
        [self.locationManager startRangingBeaconsInRegion:beaconRegion];
    }];
    
    _monitoringStarted = YES;
    
    RV_LDEBUG(@"Beacon monitoring/ranging started for regions: %@", self.beaconRegions);
}

- (void)stopMonitoring {
    for (CLRegion *region in _locationManager.monitoredRegions) {
        if ([region isKindOfClass:[CLBeaconRegion class]]) {
            [_locationManager stopMonitoringForRegion:region];
            [_locationManager stopRangingBeaconsInRegion:(CLBeaconRegion *)region];
        }
    }

    _monitoringStarted = NO;
    
    RV_LDEBUG(@"Beacon monitoring/ranging stopped for regions: %@", self.beaconRegions);
}

- (void)startMonitoringForRegions:(NSArray *)regions {
    for (CLBeaconRegion *region in regions) {
        region.notifyEntryStateOnDisplay = YES;
        [_locationManager startMonitoringForRegion:region];
    }
    
    RV_LDEBUG(@"Beacon monitoring started for regions: %@", regions);
}

- (void)stopMonitoringForAllSpecificRegions {
    for (CLRegion *region in _locationManager.monitoredRegions) {
        if ([region isKindOfClass:[CLBeaconRegion class]]) {
            CLBeaconRegion *beaconRegion = (CLBeaconRegion *)region;
            if (beaconRegion.major && beaconRegion.minor) {
                [_locationManager stopMonitoringForRegion:region];
                
                RV_LDEBUG(@"Beacon monitoring stopped for region: %@", region);
            }
        }
    }
}

#pragma mark - Helper Methods

- (void)setupBeaconRegionsForUUIDs:(NSArray *)UUIDs {
    [self.beaconRegions removeAllObjects];
    
    [UUIDs enumerateObjectsUsingBlock:^(NSUUID *UUID, NSUInteger idx, BOOL *stop) {
        CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:UUID identifier:UUID.UUIDString];
        beaconRegion.notifyEntryStateOnDisplay = YES;
        [self.beaconRegions addObject:beaconRegion];
    }];
}

- (void)setupBeaconRegions {
    [self.beaconRegions enumerateObjectsUsingBlock:^(CLBeaconRegion *beaconRegion, NSUInteger idx, BOOL *stop) {
        beaconRegion.notifyEntryStateOnDisplay = YES;
    }];
}

- (NSString *)identifierForUUID:(NSUUID *)UUID major:(CLBeaconMajorValue)major minor:(CLBeaconMinorValue)minor {
    return [NSString stringWithFormat:@"%@-%u-%u", UUID.UUIDString, major, minor];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (manager == _locationManager && status == kCLAuthorizationStatusAuthorizedAlways && _monitoringStarted) {
        RV_LTRACE(@"Location manager always authorization granted");
        [self startMonitoring];
    } else {
        RV_LDEBUG(@"Location manager always authorization revoked");
    }
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLRegion *)region {
    
    NSMutableSet *regions = [NSMutableSet set];
    [beacons enumerateObjectsUsingBlock:^(CLBeacon *beacon, NSUInteger idx, BOOL *stop) {
        CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:beacon.proximityUUID major:beacon.major.integerValue minor:beacon.minor.integerValue identifier:[self identifierForUUID:beacon.proximityUUID major:beacon.major.integerValue minor:beacon.minor.integerValue]];
        [regions addObject:beaconRegion];
    }];
    
    if ([regions isEqualToSet:self.currentRegions]) {
        return;
    } else {
        NSMutableSet *enteredRegions = [NSMutableSet setWithSet:regions];
        [enteredRegions minusSet:self.currentRegions];
        
        NSMutableSet *exitedRegions = [NSMutableSet setWithSet:self.currentRegions];
        [exitedRegions minusSet:regions];
        
        self.currentRegions = regions;

        for (CLBeaconRegion *region in exitedRegions) {
            [self.delegate regionManager:self didExitRegion:region];
            RV_LTRACE(@"Exited beacon region: %@", region);
        }
        
        for (CLBeaconRegion *region in enteredRegions) {
            [self.delegate regionManager:self didEnterRegion:region];
            RV_LTRACE(@"Entered beacon region: %@", region);
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error {
#if TARGET_IPHONE_SIMULATOR
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        RV_LWARN(@"The iOS Simulator does not support monitoring for beacons. To simulate a beacon use the [Rover simulateBeaconWithUUID:major:minor:] method. See http://dev.roverlabs.co/v1.0/docs/getting-started#simulate-a-beacon for more details.");
    });
#else
    RV_LWARN(@"Monitoring failed for region: %@", region);
    RV_LDEBUG(@"Currently monitored regions: %@", manager.monitoredRegions);
#endif
}

@end