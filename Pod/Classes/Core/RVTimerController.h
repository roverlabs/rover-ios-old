//
//  RVTimerController.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-23.
//
//

#import <Foundation/Foundation.h>

@protocol RVTimerControllerDelegate;

/** This class can create NSTimers that live beyond the application life cycle. If a timer reaches its end while the application is in the background or has been terminted, it will fire the next time application is launched.
 */
@interface RVTimerController : NSObject

@property (nonatomic, weak) id<RVTimerControllerDelegate> delegate;

/** Schedules a new timer.
 
 @param interval Time in seconds to fire the timer.
 @param identifier A unique identifier for the timer. This will passed back in the delegate when the timer fires to help identify the timer.
 */
- (void)scheduleTimerWithInterval:(NSTimeInterval)interval identifier:(id<NSCopying>)identifier;

/** Invalidates a timer given its unique identifier.
 
 @param identifier The unique identifier of the timer to be invalidated.
 */
- (void)invalidateTimerWithIdentifier:(id<NSCopying>)identifier;

/** Returns the scheduled date to fire the timer with given identifier.
 
 @param identifier The unique identifier of the scheduled timer.
 */
- (NSDate *)fireDateForTimerWithIdentifier:(id<NSCopying>)identifier;

/** Returns the scheduled NSTimer with given identifier.
 
 @param identifier The unique identifier of the timer to be returned.
 */
- (NSTimer *)timerWithIdentifier:(id<NSCopying>)identifier;

@end


@protocol RVTimerControllerDelegate <NSObject>

/** Called when the timer has finished.
 
 @param controller The instance of the timer controller calling the delegate.
 @param identifier The unique identifier of the timer that has finished.
 */
- (void)timerController:(RVTimerController *)controller didFireTimerWithIdentifier:(id<NSCopying>)identifier;

@end
