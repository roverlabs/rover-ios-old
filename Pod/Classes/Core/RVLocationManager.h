//
//  RVLocationManager.h
//  Pods
//
//  Created by Ata Namvari on 2015-10-21.
//
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@protocol RVLocationManagerDelegate;

/** This class is responsible for tracking user location.
 */
@interface RVLocationManager : NSObject


/** The delegate that gets notified of location changes.
 */
@property (nonatomic, weak) id<RVLocationManagerDelegate> delegate;

/** A threshold in kilometers to fire location change events.
 */
@property (nonatomic, assign) NSInteger threshold;

/** Last known location.
 */
@property (nonatomic, readonly) CLLocation *lastUpdatedLocation;

/** Start monitoring for location changes.
 */
- (void)startMonitoringLocationUpdates;

/** Stop monitoring for location changes.
 */
- (void)stopMonitoringLocationUpdates;

@end


@protocol RVLocationManagerDelegate <NSObject>

/** Called when user location changes by more than `threshold` kilometers.
 
 @param manager The location manager instance thats calling the delegate method.
 @param location The user's new updated location.
 */
- (void)locationManager:(RVLocationManager *)manager didChangeLocation:(CLLocation *)location;

@end