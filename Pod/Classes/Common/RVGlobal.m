//
//  RVGlobal.m
//  Pods
//
//  Created by Ata Namvari on 2015-12-03.
//
//

#import <Foundation/Foundation.h>
#import "RVGlobal.h"

BOOL rvLoggingEnabled = YES;
RVLogLevel rvLogLevel = RVLogLevelWarn;