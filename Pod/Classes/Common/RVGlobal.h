//
//  RVGlobal.h
//  Pods
//
//  Created by Ata Namvari on 2015-12-02.
//
//

/**
 * Represents the possible log levels.
 */
typedef NS_ENUM(NSInteger, RVLogLevel) {
    /**
     * Undefined log level.
     */
    RVLogLevelUndefined = -1,
    
    /**
     * No log messages.
     */
    RVLogLevelNone = 0,
    
    /**
     * Log error messages.
     */
    RVLogLevelError = 1,
    
    /**
     * Log warning messages.
     */
    RVLogLevelWarn = 2,
    
    /**
     * Log informative messages.
     */
    RVLogLevelInfo = 3,
    
    /**
     * Log debugging messages.
     */
    RVLogLevelDebug = 4,
    
    /**
     * Log detailed tracing messages.
     */
    RVLogLevelTrace = 5
};


#define RV_LEVEL_LOG_THREAD(level, levelString, fmt, ...) \
do { \
if (rvLoggingEnabled && rvLogLevel >= level) { \
NSString *thread = ([[NSThread currentThread] isMainThread]) ? @"M" : @"B"; \
NSString *method = rvLogLevel > RVLogLevelInfo ? [NSString stringWithFormat:@"%s [Line %d] ", __PRETTY_FUNCTION__, __LINE__] : @""; \
NSLog((@"Rover - [%@] [%@] => %@" fmt), levelString, thread, method, ##__VA_ARGS__); \
} \
} while(0)


#ifdef DEBUG
#   define RV_LEVEL_LOG RV_LEVEL_LOG_THREAD
#else
#   define RV_LEVEL_LOG(...)
#endif

extern BOOL rvLoggingEnabled; // Default is YES
extern RVLogLevel rvLogLevel; // Default is RVLogLevelError


#define RV_LTRACE(fmt, ...) RV_LEVEL_LOG(RVLogLevelTrace, @"T", fmt, ##__VA_ARGS__)
#define RV_LDEBUG(fmt, ...) RV_LEVEL_LOG(RVLogLevelDebug, @"D", fmt, ##__VA_ARGS__)
#define RV_LINFO(fmt, ...) RV_LEVEL_LOG(RVLogLevelInfo, @"I", fmt, ##__VA_ARGS__)
#define RV_LWARN(fmt, ...) RV_LEVEL_LOG(RVLogLevelWarn, @"W", fmt, ##__VA_ARGS__)
#define RV_LERR(fmt, ...) RV_LEVEL_LOG(RVLogLevelError, @"E", fmt, ##__VA_ARGS__)

#define RVLOG RV_LDEBUG

// constants

#ifdef _RV_VERSION
#define RV_VERSION @ _RV_VERSION
#else
#define RV_VERSION @ "3.0.12"
#endif
